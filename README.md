Instance sets for Logic-based Benders
=====================================

* planningandscheduling (planningandscheduling.zip):

  Source: http://web.tepper.cmu.edu/jnh/instances.htm
  J. N. Hooker's benchmark sets

* Rabadi-2006 (Rm-Cmax-MR-Small-Instances-Data-Rabadi-2006.zip):
* Arnaout-2010 (Rm-Cmax-ACO-Arnaout-2010.zip):

  Source: https://sites.wp.odu.edu/schedulingresearch/paper/

  Instances used by Tran & Beck, ECAI 2012. Originally from:

  Rabadi, G., Moraga, R., and Al-Salem, A. (2006), 'Heuristics for the Unrelated Parallel Machine Scheduling Problem with Setup Times', Journal of Intelligent Manufacturing, Vol. 17, p. 85 – 97.
  Arnaout, J-P., Rabadi, G. and Musa, R. (2010) 'A Two-stage Ant Colony Optimization to Minimize the Makespan on Unrelated Parallel Machines with Sequence-Dependent Setup Times', Journal of Intelligent Manufacturing, Vol. 21, No. 6, P. 693 – 701

* sscplp: Single Source Capacitated Plant Location Problem
  
  Source: http://www-eio.upc.es/~elena/sscplp/index.html
