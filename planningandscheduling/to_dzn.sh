#!/bin/sh
SRC=$1
DEST=$2

if [ ! -d $SRC ]
then
  echo "ERROR: Source directory ${SRC} not found (or not a directory)."
  exit 1
fi

if [ ! -d $DEST ]
then
  echo "Creating ${DEST}."
  mkdir ${DEST}
fi

for f in ${SRC}/*.dat
do
  echo "Processing ${f}."
  ./dat_to_dzn.py < ${f} > ${DEST}/`basename ${f} .dat`.dzn
done
