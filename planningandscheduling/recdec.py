#!/usr/bin/env python
## Helper structures for simple recursive descent parsers
import re
import sys

class ParseError(Exception):
  def __init__(self, value):
    self.value = value
  def __str__(self):
    return repr(self.value)

tok_re = re.compile(r'//[^\n]*|\w+|[;<>\[\]=]')

class Toks:
  def __init__(self, gen):
    self.gen = gen
    self.curr = None
    self.is_eof = False

  def peek(self):
    if self.is_eof:
      return None

    if self.curr is not None:
      return self.curr
    while self.curr is None or self.curr.startswith('//'):
      try:
        self.curr = self.gen.next()
      except StopIteration:
        self.is_eof = True
        return None
    return self.curr

  def next(self):
    if self.is_eof:
      raise StopIteration
    
    elt = self.peek()
    self.curr = None
    if elt is None:
      raise StopIteration
    return elt
    
  def drop(self):
    self.next()

  def empty(self):
    self.peek()
    return self.is_eof
  

def tokenize(infile):
  return Toks(t for t in tok_re.findall(infile.read()))
  
def consume(toks, val):
  elt = toks.next()
  if elt != val:
    raise ParseError("Expected {0}, got {1}.".format(val, elt))

def parse_value(toks):
  elt = toks.next()
  if elt == '[':
    # Parsing an array
    l = []
    elt = toks.peek()
    while elt != ']':
      if elt == ',':
        toks.drop()
      l.append(parse_value(toks))
      elt = toks.peek()
    toks.drop()
    return l
  elif elt == '<':
    # Parsing a tuple
    l = []
    elt = toks.peek()
    while elt != '>':
      l.append(parse_value(toks))
      elt = toks.peek()
    toks.drop()
    return tuple(l)
  else:
    return int(elt)
