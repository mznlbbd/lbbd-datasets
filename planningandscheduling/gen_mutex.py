#!/usr/bin/env python
## Given an optimal solution, generate some mutual exclusion constraints
## eliminating the given solution
import sys
import recdec
import random
import argparse

(ID, ASSIGN, START, END) = (0, 1, 2, 3)

MUTEX_COUNT = 2
MUTEX_SZ = 2

def make_argparser():
  parser = argparse.ArgumentParser(description='Generate mutual exclusion constraints.')
  parser.add_argument('clique_count', metavar='N', type=int,
                      help='number of exclusion constraints')
  parser.add_argument('clique_sz', metavar='K', type=int,
                      help='exclusion size')
  parser.add_argument('fname', metavar='input file', nargs='?',
                      help='input file')
  parser.add_argument('--set', dest='use_set', action='store_true',
                      help='use set of int, not array of bool')
  return parser
 
def gen_prec(jobs):
  ## Sort jobs by [start, end)
  starts_ord = sorted(jobs, key=lambda x : x[START])
  ends_ord = sorted(jobs, key=lambda x : x[END])
  
  si = 0
  ei = 0

  mutex_sets = []
  disjoint_sets = [ ]

  curr = set()
  block = set()
  maximal = False

  while si < len(starts_ord):
    if starts_ord[si][START] < ends_ord[ei][END]:
      maximal = True
      curr.add(starts_ord[si][ID])
      si += 1
    else:
      if maximal:
        mutex_sets.append(list(curr))
        if len(block) == 0:
          disjoint_sets.append(list(curr))
          block = curr.copy()
      
      elt = ends_ord[ei][ID]
      maximal = False
      curr.remove(elt)
      if elt in block:
        block.remove(elt)
      ei += 1

  if maximal:
    mutex_sets.append(curr)

  return (mutex_sets, disjoint_sets)
    
def parse_opt(infile):
  toks = recdec.tokenize(infile)
  sol = { }

  keys = { "assign", "starts", "ends",  "sched_cost"}

  while not toks.empty():
    curr = toks.next()

    if curr in keys:
      toks.drop()
      sol[curr] = recdec.parse_value(toks)

  return sol

if __name__ == '__main__':
  argparser = make_argparser()
  args = argparser.parse_args()

  if args.fname is not None:
    infile = open(args.fname, 'r')
  else:
    infile = sys.stdin

  sol = parse_opt(infile)

  jobs = [ (j+1, sol["assign"][j], sol["starts"][j], sol["ends"][j])
           for j in xrange(len(sol["assign"])) ]
  mutex, disj = gen_prec(jobs)

  def make_clique(m):
    return random.sample(m, min(args.clique_sz, len(m)))

  clique_num = min(args.clique_count, len(disj))
  mutices = [ make_clique(m) for m in random.sample(mutex, clique_num) ]

  #print "num_precedences = 0;"
  #print "precs = [| |];"

  print "mutex_count = {0};".format(len(mutices))
  if args.use_set:
    def show_set(s):
      return "{" + ",".join(map(str, sorted(s))) + "}"
    print "mutex = [{0}];".format(",".join(map(show_set, mutices)))
  else:
    def b_str(b):
      return "true" if b else "false"
    def show_mutex(s):
      return ",".join(map(b_str, [j in m for j in xrange(len(jobs))]))
    print "mutex = [|{0}|]".format("|".join(map(show_mutex,mutices)))
