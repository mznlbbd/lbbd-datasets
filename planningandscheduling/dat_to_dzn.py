#!/usr/bin/env python
import re
import sys

class ParseError(Exception):
  def __init__(self, value):
    self.value = value
  def __str__(self):
    return repr(self.value)

tok_re = re.compile(r'//[^\n]*|\w+|[;<>\[\]=]')

class Toks:
  def __init__(self, gen):
    self.gen = gen
    self.curr = None
    self.is_eof = False

  def peek(self):
    if self.is_eof:
      return None

    if self.curr is not None:
      return self.curr
    while self.curr is None or self.curr.startswith('//'):
      try:
        self.curr = self.gen.next()
      except StopIteration:
        self.is_eof = True
        return None
    return self.curr
g
  def next(self):
    if self.is_eof:
      raise StopIteration
    
    elt = self.peek()
    self.curr = None
    if elt is None:
      raise StopIteration
    return elt
    
  def drop(self):
    self.next()

  def empty(self):
    self.peek()
    return self.is_eof
  

def tokenize(infile):
  return (t for t in tok_re.findall(infile.read()))
  
def consume(toks, val):
  elt = toks.next()
  if elt != val:
    raise ParseError("Expected {0}, got {1}.".format(val, elt))

def parse_value(toks):
  elt = toks.next()
  if elt == '[':
    # Parsing an array
    l = []
    elt = toks.peek()
    while elt != ']':
      l.append(parse_value(toks))
      elt = toks.peek()
    toks.drop()
    return l
  elif elt == '<':
    # Parsing a tuple
    l = []
    elt = toks.peek()
    while elt != '>':
      l.append(parse_value(toks))
      elt = toks.peek()
    toks.drop()
    return tuple(l)
  else:
    return elt

def parse_dat(toks):
  parsed = {}
  
  while True:
    try:
      key = toks.next()
      consume(toks, "=");
      val = parse_value(toks);
      consume(toks, ";");
      parsed[key] = val; 
    except StopIteration:
      break
  return parsed

def to_dzn(data):
  jobmach = [list(x) for x in zip(*data["machjob"])]
  mzndata = {
      "job_count" : data["JobCount"],
      "machine_count" : data["MachineCount"],
      "capacities" : data["Limit"],
      "release" : [job[0] for job in data["job"]],
      "deadline" : [job[1] for job in data["job"]],
      "duration" : [ [ mach[0] for mach in job ] for job in jobmach ],
      "resource" : [ [ mach[1] for mach in job ] for job in jobmach ],
      "cost" : [ [ mach[2] for mach in job ] for job in jobmach ] }
  return mzndata

def print_dzn(mzndata, outfile):
  for key in mzndata:
    print >>outfile, key, "=",
    val = mzndata[key]
    if isinstance(val, list):
      if isinstance(val[0], list):
        print >>outfile, "[|{0}|]".format("|".join([", ".join(row) for row in val])),
      else:
        print >>outfile, "[{0}]".format(", ".join(val)),
    else:
      print >>outfile, val,
    print >>outfile, ";"

if __name__ == '__main__':
  toks = Toks(tokenize(sys.stdin))
  data = parse_dat(toks)
  mzndata = to_dzn(data)
  print_dzn(mzndata, sys.stdout)
