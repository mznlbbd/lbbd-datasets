#!/usr/bin/env python
import re
import sys

## Quick boilerplate for recursive descent parsing
class ParseError(Exception):
  def __init__(self, value):
    self.value = value
  def __str__(self):
    return repr(self.value)

#tok_re = re.compile(r'\w+|[;<>\[\]=]')
tok_re = re.compile(r'\d+')

class Toks:
  def __init__(self, gen):
    self.gen = gen
    self.curr = None
    self.is_eof = False

  def peek(self):
    if self.is_eof:
      return None

    if self.curr is not None:
      return self.curr
    try:
      self.curr = self.gen.next()
      return self.curr
    except StopIteration:
      self.is_eof = True
      return None

  def next(self):
    if self.is_eof:
      raise StopIteration
    
    elt = self.peek()
    self.curr = None
    if elt is None:
      raise StopIteration
    return elt
    
  def drop(self):
    self.next()

  def empty(self):
    self.peek()
    return self.is_eof
  

def tokenize(infile):
  return (t for t in tok_re.findall(infile.read()))
  
def consume(toks, val):
  elt = toks.next()
  if elt != val:
    raise ParseError("Expected {0}, got {1}.".format(val, elt))

def next_int(toks):
  return int(toks.next())

## Parsing the data file
def parse_dat(toks):
  toks.drop() # First element is a maximum horizon (I think)
  num_machines = next_int(toks)
  num_jobs = next_int(toks)

  duration = [ [ toks.next() for j in xrange(num_machines) ] for i in xrange(num_jobs) ]

  setup = [
    [ [ toks.next() for k in xrange(num_jobs) ] for j in xrange(num_jobs) ]
    for i in xrange(num_machines) ]

  return {
    "num_machines" : repr(num_machines),
    "num_jobs" : repr(num_jobs),
    "duration" : duration,
    "setup" : setup
  }
  
def print_dzn(mzndata, outfile):
  for key in mzndata:
    print >>outfile, key, "=",
    val = mzndata[key]
    if isinstance(val, list):
      if isinstance(val[0], list):
        if isinstance(val[0][0], list):
          print >>outfile, "array3d(1..{0}, 1..{1}, 1..{2}, [{3}])".format(
            len(val), len(val[0]), len(val[0][0]),
            ", ".join([elt for frame in val for row in frame for elt in row])
          ),
        else:
          print >>outfile, "[|{0}|]".format("|".join([", ".join(row) for row in val])),
      else:
        print >>outfile, "[{0}]".format(", ".join(val)),
    else:
      print >>outfile, val,
    print >>outfile, ";"

if __name__ == '__main__':
  toks = Toks(tokenize(sys.stdin))
  mzndata = parse_dat(toks)
  print_dzn(mzndata, sys.stdout)
