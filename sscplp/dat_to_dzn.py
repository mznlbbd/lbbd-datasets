#!/usr/bin/env python
import random
import re
import sys

## Quick boilerplate for recursive descent parsing
class ParseError(Exception):
  def __init__(self, value):
    self.value = value
  def __str__(self):
    return repr(self.value)

#tok_re = re.compile(r'\w+|[;<>\[\]=]')
tok_re = re.compile(r'[0-9.]+')

class Toks:
  def __init__(self, gen):
    self.gen = gen
    self.curr = None
    self.is_eof = False

  def peek(self):
    if self.is_eof:
      return None

    if self.curr is not None:
      return self.curr
    try:
      self.curr = self.gen.next()
      return self.curr
    except StopIteration:
      self.is_eof = True
      return None

  def next(self):
    if self.is_eof:
      raise StopIteration
    
    elt = self.peek()
    self.curr = None
    if elt is None:
      raise StopIteration
    return elt
    
  def drop(self):
    self.next()

  def empty(self):
    self.peek()
    return self.is_eof
  

def tokenize(infile):
  return (t for t in tok_re.findall(infile.read()))
  
def consume(toks, val):
  elt = toks.next()
  if elt != val:
    raise ParseError("Expected {0}, got {1}.".format(val, elt))

def next_int(toks):
  return int(toks.next())

def trunc(tok):
  return repr(int(float(tok)))

## Parsing the data file
def parse_dat(toks):
  num_clients = next_int(toks)
  num_plants = next_int(toks)
  allocation = [ [ int(trunc(toks.next())) for j in xrange(num_plants) ]
      for i in xrange(num_clients) ]
  demand = [ int(trunc(toks.next())) for i in xrange(num_clients) ]
  open_cost = [ int(trunc(toks.next())) for j in xrange(num_plants) ]
  capacity = [ int(trunc(toks.next())) for j in xrange(num_plants) ]

  ret = {
    "num_clients" : repr(num_clients),
    "num_plants" : repr(num_plants),
    "alloc_cost" : allocation,
    "demand" : demand,
    "open_cost" : open_cost,
    "capacity" : [int(c) for c in capacity],

  }
  
  if len(sys.argv) > 1:
    maxa = max(int(c) for cs in allocation for c in cs)
    mina = min(int(c) for cs in allocation for c in cs)
  
    def scale(c, correlated):
      if correlated:
        return int(30*(c-mina)/(maxa-mina) + 15) + random.randint(-5,5)
      return random.randint(10,50)

    ret["max_distance"] = int(sys.argv[1])
    ret["vehicle_cost"] = int(sys.argv[2])
    ret["distance"] = [[scale(int(c), sys.argv[3]=='correlated') for c in cs] for cs in allocation]
    ret["capacity"] = [3*int(c)/2 for c in capacity]

  return ret
  
def print_dzn(mzndata, outfile):
  for key in mzndata:
    print >>outfile, key, "=",
    val = mzndata[key]
    if isinstance(val, list):
      if isinstance(val[0], list):
        if isinstance(val[0][0], list):
          print >>outfile, "array3d(1..{0}, 1..{1}, 1..{2}, [{3}])".format(
            len(val), len(val[0]), len(val[0][0]),
            ", ".join([repr(elt) for frame in val for row in frame for elt in row])
          ),
        else:
          print >>outfile, "[|{0}|]".format("|".join([", ".join(repr(e) for e in row) for row in val])),
      else:
        print >>outfile, "[{0}]".format(", ".join(repr(e) for e in val)),
    else:
      print >>outfile, val,
    print >>outfile, ";"

if __name__ == '__main__':
  toks = Toks(tokenize(sys.stdin))
  mzndata = parse_dat(toks)
  print_dzn(mzndata, sys.stdout)
